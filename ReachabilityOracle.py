'''This module is part of the bachelor thesis of Lorenzo Melchior.
It provides a ReachabilityOracle as presented by Mulzer et al.  
and some testing and comparison tools.
'''
import sys

import timeit
from datetime import datetime
import math
from queue import Queue

import numpy as np
import pandas as pd
pd.options.mode.chained_assignment = None
import networkx as nx
import matplotlib.pyplot as plt
from tqdm import tqdm


class TransmissionGraph(nx.DiGraph):
    '''Base class for transmission class.
    Supports generating random data and drawing the graph.
    Input nodes with attributes "radius", "x" and "y".
    If not initialized randomly, use the add_edges method,
    to let the edges be generated.
    '''
    
    
    def __init__(self):
        super().__init__()
        
    def get_radii(self) -> list:
        '''Returns the radii for all nodes
        '''
        return [self.nodes[node]['radius'] for node in self.nodes]
        
                    
    def normalize_radii(self) -> None:
        '''Normalizes the radii, so the smallest is 1.
        Does not modify coordinates
        '''
        radii = np.array([self.nodes[i]['radius'] for i in self.nodes])
        smallest = np.sort(radii)[0]
        radii /= smallest
        for node, normalized_radius in zip(self.nodes, radii):
            self.nodes[node]['radius'] = normalized_radius

    def add_edges(self) -> None:
        '''Adding edges based on the radii and coordinates
        of the vertices
        '''
        for nodeA in self.nodes:
            for nodeB in self.nodes:
                if nodeA == nodeB:
                    continue
                elif self.reaches(nodeA, nodeB):
                    self.add_edge(nodeA, nodeB)
    
    def initialize_randomly(self, size: int, seed: int = None) -> None:
        '''Initializes $size random vertices and adds edges
        '''
        if seed:
            np.random.seed(seed)
            
        self.CENTER = 100
        self.add_nodes_from([(x,{'x': abs(np.random.normal(self.CENTER,100)), 
                                 'y': abs(np.random.normal(self.CENTER,100)), 
                                 'radius': abs(np.random.normal(10,2.4))}) 
                             for x in range(size)])
        
        self.normalize_radii()
        self.add_edges()
        
        
    def euclidean_distance(self, v1: int, v2: int) -> float:
        '''Calculates the euclidean distance between two vertices
        '''
        return np.sqrt((self.nodes[v1]['x']-self.nodes[v2]['x'])**2 + 
                       (self.nodes[v1]['y']-self.nodes[v2]['y'])**2)
        
    def reaches(self, v1: int, v2: int) -> True:
        '''Returns TRUE if v2 is in radius of v1'''
        return self.euclidean_distance(v1, v2) <= self.nodes[v1]['radius']
        
    def draw(self, draw_arrows: bool = True):
        '''Draws an image of the graph.
        '''
        
        plt.close('all')
        fig, ax = plt.subplots()
        ax.set(xlabel='x',ylabel='y')
        plt.rcParams["figure.figsize"] = (16,16)
        
        LIMIT = np.ceil(max([max([s.nodes[node]['x'],s.nodes[node]['y']]) for node in s.nodes]))
        plt.xlim(0, self.CENTER*2)
        plt.ylim(0, self.CENTER*2)
        
        x = [self.nodes[index]['x'] for index in range(len(self))]
        y = [self.nodes[index]['y'] for index in range(len(self))]
        
        if draw_arrows:
            for nodeA, nodeB in self.edges:
                plt.arrow(x=self.nodes[nodeA]['x'],
                          y=self.nodes[nodeA]['y'],
                          dx=(self.nodes[nodeB]['x']-self.nodes[nodeA]['x']),
                          dy=(self.nodes[nodeB]['y']-self.nodes[nodeA]['y']),
                          head_width = 1)
        
        
        for node in self.nodes:
            cir = plt.Circle((self.nodes[node]['x'], 
                        self.nodes[node]['y']), 
                        self.nodes[node]['radius'], 
                        color='purple',
                        fill=True,
                        alpha=0.3,
                        ec='black')
            ax.add_patch(cir)
            
        ax.scatter(x,y,s=40,color='red')
    
        
        plt.show()
        
        
        
def BFS(G: TransmissionGraph, p: int, q: int) -> bool:
    '''Breadth-first search implementation for TransmissionGraph'''
    
    already_seen = []
    queue = Queue()
    queue.put(p)
    
    while not queue.empty():
        
        next_vertex = queue.get()
        already_seen.append(next_vertex)
        
        for vertex in [item for _, item in G.edges(next_vertex)]:
            if not vertex in already_seen:
                queue.put(vertex)
                already_seen.append(vertex)    

        if next_vertex == q:
            return True
        
    return False


class ReachabilityMatrix():
    '''Reachability Matrix datastructure.'''
        
    def __init__(self, G: TransmissionGraph) -> None:
        self.G = G
        self.MATRIX = np.zeros([len(G),len(G)], dtype=bool)
        self.initialize()
        
    def __getitem__(self, key: int) -> bool:
        '''So item can be called by key'''
        return self.MATRIX[key]
        
    def initialize(self) -> None:
        '''Calculates all reachability relations'''
        for n in tqdm(range(len(self.G))):
            for m in range(len(self.G)):
                self.MATRIX[n][m] = BFS(self.G, n, m)
                
    def query(self, p, q) -> bool:
        '''Return TRUE if p can reach q'''
        return self.MATRIX[p][q]
    
    
    
class Grid():
    '''Grid class supporting the ReachabilityOracle`s short path subprogram.
    '''
    
    def __init__(self, G: TransmissionGraph, levels: int, alpha) -> None:
        
        self.G = G
        self.n = len(G)
        self.levels = levels
        self.alpha = alpha
        self.find_representatives()
        self.add_reachability_informations()
    
    
    def find_representatives(self) -> None:
        '''Find representatives for each cell, if available'''
        self.cell_sizes = [2**level / np.sqrt(2) for level in range(self.levels+1)]
        max_x = max(self.G.nodes[node]['x'] for node in self.G.nodes)
        max_y = max(self.G.nodes[node]['y'] for node in self.G.nodes)
        
        representatives = [-1*np.ones((math.ceil(max_x/self.cell_sizes[level]),
                                       math.ceil(max_y/self.cell_sizes[level]))) 
                           for level in range(self.levels+1)]
        
        for level in range(self.levels+1):
            
            for node in self.G.nodes:
                radius = self.G.nodes[node]['radius']
                if radius >= 2**level and radius < 2**(level+1):
                    x_cell = math.floor(self.G.nodes[node]['x'] / self.cell_sizes[level])
                    y_cell = math.floor(self.G.nodes[node]['y'] / self.cell_sizes[level])
                    representatives[level][x_cell][y_cell] = node
            
        self.representatives = [np.sort(np.unique(representatives[level])[1:]) for level in range(self.levels+1)]
            
        
    def are_in_neighbouring_cells(self, v1: int, v2: int, level: int) -> bool:
        '''Calculates the centroid of the cells the vertices are in
        and returns if the distance between is small enough so the cells 
        considered to be in a neighbourhood.
        '''
        cs = self.cell_sizes[level]
        c1 = np.array((math.floor(self.G.nodes[v1]['x']/cs)+(cs/2),math.floor(self.G.nodes[v1]['x']/cs)+(cs/2)))
        c2 = np.array((math.floor(self.G.nodes[v2]['x']/cs)+(cs/2),math.floor(self.G.nodes[v2]['x']/cs)+(cs/2)))
        
        return np.sqrt(((c1-c2)**2).sum()) <= 2**(level+1)*self.n**(1-self.alpha)
        
        
    def add_reachability_informations(self):
        '''Creates sorted lists of representatives, for each vertex'''
        self.n = len(self.G)
        self.p_to_r = np.array([[self.p_to_repr(p, level) 
                                     for p in self.G.nodes] 
                                         for level in range(self.levels+1)])
        self.r_to_p = np.array([[self.repr_to_p(p, level) 
                                     for p in self.G.nodes] 
                                         for level in range(self.levels+1)])
        
        
    def p_to_repr(self, p: int, level: int) -> np.array:
        '''finds representatives reachable from vertex p within the neighbourhood'''
        nearby_r = np.array([])
        for r in self.representatives[level]:
            if self.are_in_neighbouring_cells(p,r, level):
                if BFS(self.G, p, r):
                    nearby_r = np.append(nearby_r, r)
                    
        return nearby_r
                    
    def repr_to_p(self, p: int, level: int) -> np.array:
        '''finds representatives that can reach vertex p within the neighbourhood'''
        nearby_r = np.array([])
        for r in self.representatives[level]:
            if self.are_in_neighbouring_cells(p,r, level):
                if BFS(self.G, r, p):
                    nearby_r = np.append(nearby_r, r)
                    
        return nearby_r
    
    
    
    
class ReachabilityOracle(Grid):
    '''Datastructure to solve reachability queries'''
        
    def __init__(self, G: TransmissionGraph, alpha = 0.1, seed: int = None) -> None:
                
        self.G = G
        if seed:
            np.random.seed(seed)
        self.psi = max(G.get_radii())
        self.alpha = alpha
        #self.alpha = self.calculate_alpha()
        self.preprocessing_long_paths()
        self.preprocessing_short_paths()
        
        
    def preprocessing_long_paths(self) -> None:
        self.subset = self.generate_subset()
        self.p_can_reach_s = np.array([[BFS(self.G,p,s) for s in self.subset] for p in self.G.nodes])
        self.s_can_reach_p = np.array([[BFS(self.G,s,p) for p in self.G.nodes] for s in self.subset])

    def preprocessing_short_paths(self) -> None:
        levels = math.floor(math.log(self.psi,2))
        self.grids = Grid(self.G,levels,self.alpha)
        
        
    def calculate_alpha(self) -> float:
        '''Calculates alpha as proposed in the paper, unused right now'''
        n = len(self.G)
        BASE = 2
        
        alpha = math.log( n**(2/3) * (math.log(self.psi,BASE)/math.log(n,BASE))**(1/3) ,BASE)
        return alpha
        
    def generate_subset(self) -> np.array:
        '''Creates random subset for long path preprocessing'''
        n = len(self.G)
        number_of_elements = math.ceil(4 * n**(self.alpha) * math.log(n, math.e))
        return np.random.choice(self.G.nodes, number_of_elements, replace=False)
    
    def query_long_paths(self, p: int, q: int) -> bool:
        '''Returns TRUE if long path subprogram detects reachability'''
        for index, reachable in enumerate(self.p_can_reach_s[p]):
            if reachable and self.s_can_reach_p[index][q]:
                return True
        return False
    
    def query_short_paths(self, p, q) -> bool:
        '''Returns TRUE if short path subprogram detects reachability'''
        for level in range(self.grids.levels+1):
            
            p_to_r = self.grids.p_to_r[level][p]
            r_to_p = self.grids.r_to_p[level][q]
            
            for pr in p_to_r:
                for rp in r_to_p:
                    if pr==rp:
                        return True
                    if rp>pr:
                        continue
            return False

    def query(self, p, q) -> bool:
        '''Returns TRUE if it thinks, that p can reach q, FALSE if not'''
        if self.query_long_paths(p,q):
            return True
        else:
            return self.query_short_paths(p,q)
        
        
class Test():
    '''Tests the three algorithmss'''
    
    def __init__(self, G: TransmissionGraph, alpha=0.1, seed: int = None) -> None:
        self.G = G
        self.seed = seed
        self.alpha = alpha
        self.results = pd.DataFrame({'Reachability Matrix': 
                                        {'Preprocessing Time': 0, 'Space usage': 0, 'Runtime': 0, 'Accuracy':1},
                                     'Breadth-first Search': 
                                        {'Preprocessing Time': 0, 'Space usage': 0, 'Runtime': 0, 'Accuracy':1},
                                     'Reachability Oracle': 
                                        {'Preprocessing Time': 0, 'Space usage': 0, 'Runtime': 0, 'Accuracy':0}})
        
    def timer(self, function, *kwarg) -> tuple:
        '''measures time for given function'''
        starttime = datetime.now()
        ret = function(*kwarg)
        endtime = datetime.now()
        return (endtime-starttime), ret
        
    def run_all_tests(self) -> None:
        '''wrapper to run all tests'''
        self.test_preprocessing()
        self.test_space_usage()
        self.test_runtime()
        self.test_accuracy()
        
    def test_preprocessing(self) -> None:
        '''Tests matrix and oracle for preprocessing time'''
        self.results['Reachability Matrix']['Preprocessing Time'], self.RM = self.timer(ReachabilityMatrix, self.G)
        self.results['Reachability Oracle']['Preprocessing Time'], self.RO = self.timer(ReachabilityOracle, self.G, self.alpha, self.seed)

    def test_space_usage(self) -> None:
        '''Saves space usage for matrix and oracle'''
        space_RM = sys.getsizeof(self.RM.MATRIX)
        
        space_RO = sys.getsizeof(self.RO.subset)
        space_RO += sys.getsizeof(self.RO.p_can_reach_s)
        space_RO += sys.getsizeof(self.RO.s_can_reach_p)
        space_RO += sys.getsizeof(self.RO.grids.p_to_r)
        space_RO += sys.getsizeof(self.RO.grids.r_to_p)
        
        self.results['Reachability Matrix']['Space usage'] = space_RM
        self.results['Reachability Oracle']['Space usage'] = space_RO
        
    def test_runtime(self) -> None:
        '''Saves runtime for all three algorithms'''
        times_RM = []
        times_BFS = []
        times_RO = []
        
        for v1 in tqdm(self.G.nodes):
            for v2 in self.G.nodes:
                time_RM, _ = self.timer(self.RM.query,v1,v2)
                times_RM.append(time_RM)
                time_BFS, _ = self.timer(BFS, self.G,v1,v2)
                times_BFS.append(time_BFS)
                time_RO, _ = self.timer(self.RO.query, v1,v2)
                times_RO.append(time_RO)
                
        self.results['Reachability Matrix']['Runtime'] = np.array(times_RM).sum()
        self.results['Breadth-first Search']['Runtime'] = np.array(times_BFS).sum()
        self.results['Reachability Oracle']['Runtime'] = np.array(times_RO).sum()
                            
            
            
    def test_accuracy(self):
        '''Saves accuracy of Oracle'''
        correct = 0
        false = 0
        
        for v1 in self.G.nodes:
            for v2 in self.G.nodes:
                if self.RO.query(v1,v2) == self.RM[v1][v2]:
                    correct+=1
                else:
                    false+=1
        
        self.results['Reachability Oracle']['Accuracy'] = correct/(correct+false)
        
        
class AlphaTest():
    '''Tests different alpha values for oracle'''
    
    def __init__(self, G: ReachabilityOracle, seed: int = None) -> None:
        self.G = G
        self.seed = seed
        self.subset_sizes = {}
        self.space_usages = {}
        self.preprocessing_times = {}
        self.runtimes = {}
        self.accuracys = {}
        
    
            
    def test_alphas(self, alphas: list) -> None:
        self.alphas = alphas
        
        for alpha in alphas:
            try:
                T = Test(self.G, alpha, self.seed)
                T.run_all_tests()
                self.space_usages[alpha] = T.results['Reachability Oracle']['Space usage']
                self.preprocessing_times[alpha] = T.results['Reachability Oracle']['Preprocessing Time']
                self.runtimes[alpha] = T.results['Reachability Oracle']['Runtime']
                self.accuracys[alpha] = T.results['Reachability Oracle']['Accuracy']
                
            except:
                self.space_usages[alpha] = None
                self.preprocessing_times[alpha] = None
                self.runtimes[alpha] = None
                self.accuracys[alpha] = None
            self.subset_sizes[alpha] = 4*len(self.G.nodes)**alpha *math.log(len(self.G.nodes))

            
    def draw(self) -> None:
        
        fig, ((ax1, ax2), (ax3, ax4)) = plt.subplots(2, 2)
        fig.suptitle('Oracle Performance')
        
        ax1.plot(self.alphas, [a.total_seconds() for a in list(self.preprocessing_times.values())], 'tab:blue')
        ax1.set(xlabel='Alpha',ylabel='Preprocessing (in seconds)')
        ax2.plot(self.alphas, list(self.space_usages.values()), 'tab:green')
        ax2.set(xlabel='Alpha',ylabel='Space usage (in bytes)')
        ax3.plot(self.alphas, [float(a.total_seconds())*10**6 for a in list(self.runtimes.values())], 'tab:red')
        ax3.set(xlabel='Alpha',ylabel='Query-time (average in mikroseconds)')
        ax4.plot(self.alphas, list(self.accuracys.values()), 'tab:blue')
        ax4.set(xlabel='Alpha',ylabel='Accuracy')
        
